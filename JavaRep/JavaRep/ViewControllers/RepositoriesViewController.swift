//
//  RepositoriesViewController.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 16/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SwiftyJSON
class RepositoriesViewController: UIViewController {

    let DISTANCE_TO_BOTTON: CGFloat = -40
    
    @IBOutlet weak var tableView: UITableView!
    
    var repositories = [Repository](){
        didSet{
            
            self.reloadTableview()
            self.downloadingData = false
        }
    }
    // Marcação da pagina atual baixada do servidor
    var currentPage = 1
    var downloadingData = false
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.reloadData()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    private func reloadTableview(){
        self.tableView.reloadData()
    }

    // Busca dados no servidor
    func reloadData(){
        if !downloadingData{
            downloadingData = true
            APIRequest.getRepositories(page: currentPage, delegate: self)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let destination = segue.destination as? PullRequestViewController else {
            return
        }
        // Setando propriedades no destino
        destination.repository = self.repositories[selectedIndex]
    }
    
    
}

// MARK: - UITableView
// MARK: Delegate
extension RepositoriesViewController:UITableViewDelegate{
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let currentYPosition = targetContentOffset.pointee.y
        let maximumOffset = scrollView.contentSize.height - scrollView.bounds.height
        
        if currentYPosition - maximumOffset >= DISTANCE_TO_BOTTON {
            self.reloadData()
        }

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        performSegue(withIdentifier: "PullRequestSegue", sender: nil)
    }
    
}

// MARK: Datasouecing
extension RepositoriesViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "RepositoryCellIdentifier"
        
        let cell:RepositoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RepositoryTableViewCell
        
        cell.setup(repository: repositories[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - API Request Delegate
extension RepositoriesViewController:APIRequestDelegate{
    
    // Retorno com sucesso da API (identifier identifica chamada)
    func successCallback(result: JSON, identifier: APIRequestIdentifier) {
        
        RepositoryController.addData(data: result) { (repositories, finish) in
            self.repositories += repositories
            if self.tableView.isHidden{
                self.tableView.isHidden = false
            }
            if !finish {
                self.currentPage += 1
            }
            
        }
        
    }
    // Retorno com erro simples de API
    func errorCallback(identifier: APIRequestIdentifier) {
        let alert = UIAlertController(title: "Erro", message: "Desculpe, ocorreu um erro inesperado. Tente novamente mais tarde.", preferredStyle: UIAlertControllerStyle.alert)
        
        if tableView.isHidden{
            let tentar = UIAlertAction(title: "Tentar", style: .default, handler: { (action) in
                self.reloadData()
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(tentar)
            alert.addAction(cancelar)
            
        }else{
            let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(ok)
        }
        
        self.navigationController?.present(alert, animated: true, completion: {
            self.downloadingData = false
        })
    }
    // Retorno de erro de conexão com internet
    func noInternetConnection(identifier: APIRequestIdentifier) {
        let alert = UIAlertController(title: "Erro", message: "Sem conexão com a internet. Tente novamente mais tarde.", preferredStyle: UIAlertControllerStyle.alert)
        if tableView.isHidden{
            let tentar = UIAlertAction(title: "Tentar", style: .default, handler: { (action) in
                self.reloadData()
            })
            let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(tentar)
            alert.addAction(cancelar)
            
        }else{
            let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(ok)
        }
        self.navigationController?.present(alert, animated: true, completion: { 
            self.downloadingData = false
        })
        
    }
    
    
}
