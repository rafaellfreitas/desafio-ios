//
//  PullRequestViewController.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SwiftyJSON


class PullRequestViewController: UIViewController {

    var repository:Repository?
    
    var pullRequests = [PullRequest](){
        didSet{
            self.reloadTableView()
        }
        
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = self.repository?.name
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.reloadData()
        
    }

    func reloadData(){
        APIRequest.getPullRequests(author: repository!.ownerName!, repository: repository!.name!, delegate: self)
    }
    
    func reloadTableView(){
        self.tableView.reloadData()
    }
}

//MARK: -UItableView
//MARK: Delegate
extension PullRequestViewController:UITableViewDelegate{
    
}


//MARK: Datasource
extension PullRequestViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "PullRequestIdentifier"
        
        let cell:PullRequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PullRequestTableViewCell
        
        cell.setup(pullRequest: pullRequests[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: pullRequests[indexPath.row].url!) else {
            return
        }
        
        // Simulador não está abrindo url segura
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}


//MARK: - APIRequestDelegate
extension PullRequestViewController:APIRequestDelegate{
   // Retorno com sucesso da API (identifier identifica chamada)
    func successCallback(result: JSON, identifier: APIRequestIdentifier) {
        
        PullRequestController.addData(data: result) { pullRequests in
            if self.tableView.isHidden{
                self.tableView.isHidden = false
            }
            self.pullRequests = pullRequests
        }
    }
    // Retorno com erro simples de API
    func errorCallback(identifier: APIRequestIdentifier) {
        let alert = UIAlertController(title: "Erro", message: "Desculpe, ocorreu um erro inesperado. Tente novamente mais tarde.", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        alert.addAction(ok)
        self.navigationController?.present(alert, animated: true, completion: nil)
        
    }
    
    // Retorno de erro de conexão com internet
    func noInternetConnection(identifier: APIRequestIdentifier) {
        
        let alert = UIAlertController(title: "Erro", message: "Sem conexão com a internet. Tente novamente mais tarde.", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        alert.addAction(ok)
        self.navigationController?.present(alert, animated: true, completion: nil)
        
    }
}

