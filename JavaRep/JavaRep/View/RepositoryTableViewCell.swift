//
//  RepositoryTableViewCell.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 16/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

    
    
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var forkLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Configurando célula
    func setup(repository:Repository){
        
        userImageView.sd_setImage(with: URL(string: repository.ownerAvatarURL!),
                                  placeholderImage: UIImage(named: "person_placeholder"),
                                  options: SDWebImageOptions.highPriority,
                                  progress: nil,
                                  completed: nil)
        titleLabel.text = repository.fullName
        descriptionLabel.text = repository.description
        starLabel.text = String(describing: repository.starsCount!)
        forkLabel.text = String(describing: repository.forksCount!)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = ""
        descriptionLabel.text = ""
        starLabel.text = ""
        forkLabel.text = ""
        
    }

}
