//
//  PullRequestTableViewCell.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SDWebImage
import TSMarkdownParser

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Configurando célula
    func setup(pullRequest:PullRequest){
        
        
        
        avatarImageView.sd_setImage(with: URL(string: pullRequest.ownerAvatarURL!),
                                  placeholderImage: UIImage(named: "person_placeholder"),
                                  options: SDWebImageOptions.highPriority,
                                  progress: nil,
                                  completed: nil)
        
        titleLabel.text = pullRequest.title
        authorLabel.text = pullRequest.ownerName
        if let date = dateFormatter.date(from: pullRequest.createdAt!){
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .none
            
            dateLabel.text = dateFormatter.string(from: date)
        }

        
        
        
        // Adição de marcação de texto
        guard let markdown = pullRequest.body else{
            descriptionLabel.text = pullRequest.body
            return
        }
        descriptionLabel.attributedText = TSMarkdownParser.standard().attributedString(fromMarkdown: markdown)
        
  
    }
    
    
    
}





