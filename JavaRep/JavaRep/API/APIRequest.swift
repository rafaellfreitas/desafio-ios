//
//  APIHelper.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 16/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import Reachability
import Alamofire
import SwiftyJSON



//MARK: - API Constants

let API_CONF_BASE_URL = "https://api.github.com"


//MARK: - API Request Identifier
// Identificador de chamadas na api vinda do controller
enum APIRequestIdentifier:Int {
    case None,NoInternetConnection,Repositories
}


//MARK: - API Request Protocol
protocol APIRequestDelegate:class
{
    func successCallback(result: JSON, identifier: APIRequestIdentifier)
    func errorCallback(identifier: APIRequestIdentifier)
    func noInternetConnection(identifier: APIRequestIdentifier)
    
    
}

//MARK: - API Request Class
class APIRequest:NSObject {

    // MARK:  Teste de Conexão com a Internet
    static func hasInternetConnection()->Bool{
        
        guard let reachability = Reachability.forInternetConnection() else{
            return false
        }
     
        return reachability.isReachable()
        
    }
    
    // MARK: Repositorios
    // Obtem repositórios conforme página solicitada
    static func getRepositories(page:Int, delegate:APIRequestDelegate){
        
        guard hasInternetConnection() else {
            delegate.noInternetConnection(identifier: .NoInternetConnection)
            return
        }
        
        let requestIdentifier:APIRequestIdentifier = .Repositories
    
        let requestAddress = "\(API_CONF_BASE_URL)/search/repositories"
        
        let parameters = ["page": "\(page)", "q": "language:Java", "sort": "stars"]
        

        Alamofire.request(requestAddress, parameters:parameters )
            .responseJSON { response in
            
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    delegate.successCallback(result: json, identifier: requestIdentifier)
                    
                case .failure(let error):
                    print(error)
                    delegate.errorCallback(identifier: requestIdentifier)
                }
        }
    }
    
    
    // MARK: PullRequests
    
    static func getPullRequests(author:String, repository:String , delegate: APIRequestDelegate){
        
        guard hasInternetConnection() else {
            delegate.noInternetConnection(identifier: .NoInternetConnection)
            return
        }
        
        
        let requestIdentifier:APIRequestIdentifier = .Repositories
        
        let requestAddress = "\(API_CONF_BASE_URL)/repos/\(author)/\(repository)/pulls"
        
        
        Alamofire.request(requestAddress)
            .responseJSON { response in
                
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    delegate.successCallback(result: json, identifier: requestIdentifier)
                    
                case .failure(let error):
                    print(error)
                    delegate.errorCallback(identifier: requestIdentifier)
                }
        }
    }
}
