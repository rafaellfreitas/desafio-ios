//
//  Repository.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import Foundation


struct Repository {
    var name: String?
    var fullName: String?
    var description: String?
    var ownerName: String?
    var ownerAvatarURL : String?
    var starsCount: Int?
    var forksCount: Int?
    
    enum Key: String {
        case name = "name"
        case fullName = "full_name"
        case description = "description"
        case owner = "owner"
        case ownerLogin = "login"
        case avatarUrl = "avatar_url"
        case starsCount = "stargazers_count"
        case forksCount = "forks_count"
    }
}


