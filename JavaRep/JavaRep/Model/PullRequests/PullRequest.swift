//
//  PullRequest.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit

struct PullRequest {
    
    var url: String?
    var title: String?
    var ownerName: String?
    var ownerAvatarURL : String?
    var body: String?
    var createdAt: String?
    var updatedAt: String?
    
    enum Key: String {
    
        case url = "html_url"
        case title = "title"
        case user = "user"
        case ownerLogin = "login"
        case avatarUrl = "avatar_url"
        case body = "body"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

}
