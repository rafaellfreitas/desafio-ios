//
//  PullRequestController.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequestController: NSObject {

    
    
    static func addData(data:JSON, completion: (_ pullRequests: [PullRequest])->())  {
        
        var result = [PullRequest]()
        
        
        for (_,item):(String, JSON) in data {
        print(item[PullRequest.Key.user.rawValue][PullRequest.Key.avatarUrl.rawValue].string)
        result.append(PullRequest(url: item[PullRequest.Key.url.rawValue].string,
                                  title: item[PullRequest.Key.title.rawValue].string,
                                  ownerName: item[PullRequest.Key.user.rawValue][PullRequest.Key.ownerLogin.rawValue].string,
                                  ownerAvatarURL: item[PullRequest.Key.user.rawValue][PullRequest.Key.avatarUrl.rawValue].string,
                                  body: item[PullRequest.Key.body.rawValue].string,
                                  createdAt: item[PullRequest.Key.createdAt.rawValue].string,
                                  updatedAt: item[PullRequest.Key.updatedAt.rawValue].string))
            
        }
        
        completion(result)
        
    }
    
    
    
    
}
