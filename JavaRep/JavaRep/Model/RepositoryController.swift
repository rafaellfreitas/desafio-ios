//
//  RepositoryController.swift
//  JavaRep
//
//  Created by Rafael Aparecido de Freitas on 17/12/17.
//  Copyright © 2017 Rafael Aparecido de Freitas. All rights reserved.
//

import UIKit
import SwiftyJSON
class RepositoryController: NSObject {

    
    static func addData(data:JSON, completion: (_ repositories: [Repository], _ finish:Bool)->())  {
        
        var result = [Repository]()
        
        let items = data["items"]
        var finish = false
        
        if items.count == 0{
            finish = true
        }
        for (_,item):(String, JSON) in items {
            
            result.append(Repository(
                name:  item[Repository.Key.name.rawValue].string,
                fullName:  item[Repository.Key.fullName.rawValue].string,
                description: item[Repository.Key.description.rawValue].string,
                ownerName: item[Repository.Key.owner.rawValue][Repository.Key.ownerLogin.rawValue].string,
                ownerAvatarURL: item[Repository.Key.owner.rawValue][Repository.Key.avatarUrl.rawValue].string,
                starsCount: item[Repository.Key.starsCount.rawValue].int,
                forksCount: item[Repository.Key.forksCount.rawValue].int))
            
        }
        
        completion(result, finish)
        
    }
    
}
